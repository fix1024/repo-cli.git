import { describe, expect, it } from 'vitest';
import { mount } from '@vue/test-utils';
import Input from '../index.vue';

describe('测试输入框组件', () => {
  it('渲染组件', () => {
    const wrapper = mount(Input, {
      slots: {
        default: 'easyest'
      }
    });

    // Assert the rendered text of the component
    expect(wrapper.text()).toContain('easyest');
  });
});
