import _Input from './index.vue';
import { withInstall } from '@easyest/utils';

export const Input = withInstall(_Input);
export default Input;
